#ifndef CONFIG_STRUCT_HPP
#define CONFIG_STRUCT_HPP

#include <netdb.h> 


typedef struct {
    struct sockaddr_in serv_addr;
    struct sockaddr_in client_addr;
    int portno;
    int sockfd;
}config_struct;

#endif