#ifndef PROTOCOL_HPP
#define PROTOCOL_HPP
#include <string>

#define SEND_STREAM "send_stream"
#define ACCEPT_STREAM "streaming_"
#define STOP_STREAM "stop_stream"

void protocol(int sockfd, char *MESSAGE, std::string msg);

#endif