#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <numeric>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h> 


#define WIDTH 640
#define HEIGHT 480

#include "header/error_.hpp"
#include "header/error_socket.hpp"
#include "header/config_struct.hpp"
#include "header/config_socket.hpp"
#include "header/frame_size.hpp"
#include "header/protocol.hpp"

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
    //socket config structre, buffer and socket result variable
    VideoCapture acquisition(0);
    Mat frame;
    int n, frame_n=0,on_connection;
    config_struct config_;
    char buffer[] = SEND_STREAM, file_name[20];
    bool ack = false, tmp;
    //vector of frame
    int imgSize = frame_size(CV_8UC3);
    if(argc < 4)
    {
        error_("Please use like: "+ String(argv[0])+" <local-ip> <client-ip> <port-number>");
    }

    config_socket(&config_,argv);
    error_socket(config_.sockfd,"ERROR opening socket.. closing program");
    socklen_t client_len = sizeof(config_.client_addr);
    if(bind(config_.sockfd,(struct sockaddr*)&(config_.serv_addr),sizeof(config_.serv_addr)) < 0)
        error_("ERROR binding socket.. closing program");
    listen(config_.sockfd,1);
    on_connection = accept(config_.sockfd,(struct sockaddr*)&(config_.client_addr),&client_len);
    error_socket(on_connection, "ERROR acception connection.. closing program");
    cout << "connection accepted" << endl;
    //send frames till death
    while(true)
    {
        bzero(buffer,sizeof(buffer));
        n = read(on_connection,buffer,sizeof(buffer));
        if(strcmp(buffer,SEND_STREAM) == 0)
        {
            tmp = acquisition.open(0);
            ack = true;
            protocol(on_connection,ACCEPT_STREAM,"ERROR can't WRITE TO sender.. closing program");
        }
        else
        {
            cout << buffer << endl;
            error_("No command recieved..");
        }
        while(ack)
        {
            frame_n++;
            bzero(buffer, sizeof(buffer));
            if(acquisition.isOpened())
            {
                acquisition >> frame;
                frame = frame.reshape(0,1);
                imgSize = frame.total()*frame.elemSize();
                n = send(on_connection,frame.data,imgSize,0);
                if(n < 0)
                    error_("ERROR sending image to target..");
                n = read(on_connection,buffer,sizeof(buffer));
                if(strcmp(buffer,SEND_STREAM) == 0)
                {
                    ack = true;
                    protocol(on_connection,ACCEPT_STREAM,"ERROR can't WRITE TO sender.. closing program");
                }
                else
                {
                    ack = false;
                    protocol(on_connection,STOP_STREAM,"ERROR can't read from sender.. closing program");
                }
            }
            else
            {
                error_("ERROR opening camera..");
            }
        }
        acquisition.release();
    }
    close(on_connection);
    return 0;
}