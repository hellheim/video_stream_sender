#include "header/config_socket.hpp"
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

void config_socket(config_struct *config_, char ** argv)
{
    config_->sockfd = socket(AF_INET,SOCK_STREAM,0);
    config_->portno = atoi(argv[3]);

    config_->serv_addr.sin_family = AF_INET;
    config_->serv_addr.sin_addr.s_addr = inet_addr(argv[1]);
    config_->serv_addr.sin_port = htons(config_->portno);

    config_->client_addr.sin_family = AF_INET;
    config_->client_addr.sin_addr.s_addr = inet_addr(argv[2]);
    config_->client_addr.sin_port = htons(config_->portno);

}